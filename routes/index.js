var express = require('express');
var router = express.Router();

router.get('/', (req, res) => {
    res.redirect('/w0')
});

router.get('/:type', (req, res) => {
    let type = req.params.type
    res.render(type)
});

module.exports = router;
